package com.sda.javagda14.rowerownia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;
    private Long time;
    private Long cost;
    private Status status;
    private LocalDateTime dateAdded;
    private LocalDateTime dateResolved;

    @ManyToOne
    @JsonIgnore
    private AppUser owner;

    @OneToMany(mappedBy = "task")
    @JsonIgnore
    private List<Item> items;
}
