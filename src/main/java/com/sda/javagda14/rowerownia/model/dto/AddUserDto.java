package com.sda.javagda14.rowerownia.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddUserDto {
    private String name;
    private String surname;

    @Size(min = 6, max = 255)
    private String password;

//    @Size(min = 6, max = 255)
//    private String passwordConfirm;

    @Email
    private String email;

    private Integer phone;
}
