package com.sda.javagda14.rowerownia.model;

public enum Status {
    PENDING,
    TO_ACCEPT,
    IN_PROGRESS,
    DONE;
}
