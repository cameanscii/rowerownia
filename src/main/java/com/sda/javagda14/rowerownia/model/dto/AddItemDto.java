package com.sda.javagda14.rowerownia.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddItemDto {
    private Long taskId;
    private String name, details;
    private Long cost;
}
