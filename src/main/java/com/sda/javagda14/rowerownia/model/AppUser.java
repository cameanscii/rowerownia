package com.sda.javagda14.rowerownia.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name, surname;

    private String password;

    private String email;

    @ManyToMany
    private Set<AppUserRole> roles;

    private Integer phone;


    @OneToMany(mappedBy = "owner")
    private List<Task> taskList;
}