package com.sda.javagda14.rowerownia.controller;


import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.service.LoginService;
import com.sda.javagda14.rowerownia.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class AppUserController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private TaskService taskService;

    @GetMapping("/login")
    public String getLoginPage() {
        return "appuser/login";
    }

    @GetMapping("/")
    public String getIndex() { return "index";
    }

    @GetMapping("/list_tasks_done")
    public String getListDone(Model model) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            List<Task> taskList =
                    taskService.getDone(taskService.getAllUserTasks(userOptional.get().getId()));
            model.addAttribute("tasks", taskList);
            return "admin/list_tasks_done";
        }
        return "redirect:/login";
    }
}
