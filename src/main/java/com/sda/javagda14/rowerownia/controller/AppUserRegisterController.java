package com.sda.javagda14.rowerownia.controller;

import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.dto.AddUserDto;
import com.sda.javagda14.rowerownia.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
public class AppUserRegisterController {

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/register")
    public String getAddView(Model model) {
        model.addAttribute("added_object", new AddUserDto());
        return "appuser/register";
    }

    @PostMapping("/register")
    public String getAddView(Model model, AddUserDto dto) {
        if (dto.getPassword().isEmpty() || dto.getPassword().length() < 5) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "Too simple password.");
            return "appuser/register";
        }

        Optional<AppUser> appUserOptional = appUserService.register(dto);
        if (!appUserOptional.isPresent()) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "User with that email already exists.");
            return "appuser/register";
        }

        return "redirect:/";
    }
}
