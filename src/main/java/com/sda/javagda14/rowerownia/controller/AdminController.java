package com.sda.javagda14.rowerownia.controller;

import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.Item;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.model.dto.AddItemDto;
import com.sda.javagda14.rowerownia.model.dto.ModifyTaskDto;
import com.sda.javagda14.rowerownia.repository.AppUserRepository;
import com.sda.javagda14.rowerownia.service.AppUserService;
import com.sda.javagda14.rowerownia.service.ItemService;
import com.sda.javagda14.rowerownia.service.LoginService;
import com.sda.javagda14.rowerownia.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private TaskService taskService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/modify/{task_id}")
    public String modify(Model model, @PathVariable(name = "task_id") Long taskId) {
        Optional<Task> optionalTask = taskService.getTask(taskId);
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            model.addAttribute("task", new ModifyTaskDto(task.getId(), task.getTime()));
            return "admin/modify";
        }
        return "redirect:/";
    }

    @PostMapping("/modify")
    public String modify(Model model, ModifyTaskDto dto) {

        Optional<Task> optionalTask = taskService.modify(dto);
        // todo: tutaj zmienilem zeby jesli NIE UDA SIE ZMIENIC TASK TO DOPIERO WRACASZ NA FORMULARZ
        if (!optionalTask.isPresent()) {
            Task task = optionalTask.get();
            model.addAttribute("task", new ModifyTaskDto(task.getId(), task.getTime()));
            return "admin/modify";
        }

        return "redirect:/task/list";
    }

    @GetMapping("/add_item/{task_id}")
    public String getAddForm(Model model, @PathVariable(name = "task_id") Long task_id) {

        Optional<Task> task = taskService.getTask(task_id);
        if (!task.isPresent()) {
            return "redirect:/";
        }
        AddItemDto item = new AddItemDto();
        item.setTaskId(task_id);
        model.addAttribute("item", item);
        return "admin/add_item";

    }

    @PostMapping("/add_item")
    public String addItem(AddItemDto dto, Model model) {

        Optional<Item> item = itemService.add(dto);
        if (!item.isPresent()) {
            model.addAttribute("item", dto);
            model.addAttribute("error_message", "Validation error!");
            return "admin/add_item";
        }

        return "redirect:/task/details/" + dto.getTaskId();
    }


    @GetMapping("/delete_item/{item_id}/{task_id}")
    public String delete(@PathVariable(name = "item_id") Long itemId, @PathVariable(name = "task_id") Long taskId) {

        Optional<Item> optionalItem = itemService.getItem(itemId);
        if (optionalItem.isPresent()) {
            itemService.delete(itemId, taskId);
            return "redirect:/task/details/" + taskId;
        }
        return "redirect:/task/details/" + taskId;
    }

    @GetMapping("/finish/{task_id}")
    public String accept(@PathVariable(name = "task_id") Long taskId) {

        Optional<Task> optionalTask = taskService.getTask(taskId);
        if (optionalTask.isPresent()) {
            taskService.finish(taskId);
            return "redirect:/task/list";
        }
        return "redirect:/task/list";
    }

    @GetMapping("/list_tasks_done")
    public String getListDone(Model model) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            if (userOptional.get().getRoles().stream().filter(appUserRole -> appUserRole.getName().equals("ROLE_ADMIN")).count() >= 1) {
                List<Task> taskList =
                        taskService.getDone(taskService.getAll());
                model.addAttribute("tasks", taskList);
                return "admin/list_tasks_done";
            }
            List<Task> taskList =
                    taskService.getDone(taskService.getAllUserTasks(userOptional.get().getId()));
            model.addAttribute("tasks", taskList);
            return "admin/list_tasks_done";
        }
        return "redirect:/login";
    }

    @GetMapping("/list_users")
    public String getListAppUsers(Model model) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            List<AppUser> users = appUserService.getAll();
            model.addAttribute("users", users);
            return "admin/list_users";
        }
        return "redirect:/login";
    }

}
