package com.sda.javagda14.rowerownia.controller;


import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.AppUserRole;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.model.dto.AddTaskToUserDto;
import com.sda.javagda14.rowerownia.model.dto.ModifyTaskDto;
import com.sda.javagda14.rowerownia.service.AppUserService;
import com.sda.javagda14.rowerownia.service.LoginService;
import com.sda.javagda14.rowerownia.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;




@Controller
@RequestMapping("/task/")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AppUserService appUserService;
    @GetMapping("/add")
    public String add(Model model) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (!userOptional.isPresent()) {
            return "redirect:/login";
        }
        AddTaskToUserDto addTaskToUserDto = new AddTaskToUserDto();
        addTaskToUserDto.setUserId(userOptional.get().getId());
        model.addAttribute("added_task", addTaskToUserDto);
        return "task/add";
    }

    @PostMapping("/add")
    public String add(Model model, AddTaskToUserDto dto) {

        Optional<Task> optionalTask = taskService.addTask(dto);
        if (!optionalTask.isPresent()) {
            model.addAttribute("added_task", dto);
            model.addAttribute("error_message", "Validation error!");
            return "task/add";
        }
        return "redirect:/";
    }

    @GetMapping("/list")
    public String getList(Model model) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            if (userOptional.get().getRoles().stream().filter(appUserRole -> appUserRole.getName().equals("ROLE_ADMIN")).count() >= 1) {
                List<Task> taskList =
                        taskService.getNotDone(taskService.getAll());
                model.addAttribute("tasks", taskList);
                return "task/list";
            }
            List<Task> taskList =taskService.getNotDone(
                    taskService.getAllUserTasks(userOptional.get().getId()));
            model.addAttribute("tasks", taskList);
            return "task/list";
        }
        return "redirect:/login";
    }
    @GetMapping("/list/{user_id}")
    public String getListById(Model model,@PathVariable(name = "user_id") Long user_id) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            List<Task> taskList = taskService.getAllUserTasks(user_id);
            model.addAttribute("tasks", taskList);
            return "task/list";
        }
        return "redirect:/login";
    }

    @GetMapping("/details/{task_id}")
    public String getDetail(Model model, @PathVariable(name = "task_id") Long taskId) {
        Optional<AppUser> userOptional = loginService.getLoggedInUser();
        if (userOptional.isPresent()) {
            Optional<Task> optionalTask = taskService.getTask(taskId);
            if (optionalTask.isPresent()) {
                Task task = optionalTask.get();
                model.addAttribute("task", task);
                model.addAttribute("cost", task.getItems().stream().mapToLong( item -> item.getCost()).sum());
                return "task/details";
            }
            return "redirect:/";
        }

        return "redirect:/login";
    }
    @GetMapping("/delete/{task_id}")
    public String delete(@PathVariable(name = "task_id") Long taskId) {

        Optional<Task> optionalTask = taskService.getTask(taskId);
        if (optionalTask.isPresent()) {
            taskService.delete(taskId);
            return "redirect:task/list";
        }
        return "redirect:/task/list";
    }

    @GetMapping("/accept/{task_id}")
    public String accept(@PathVariable(name = "task_id") Long taskId) {

        Optional<Task> optionalTask = taskService.getTask(taskId);
        if (optionalTask.isPresent()) {
            taskService.accept(taskId);
            return "redirect:/task/list";
        }
        return "redirect:/task/list";
    }
}
