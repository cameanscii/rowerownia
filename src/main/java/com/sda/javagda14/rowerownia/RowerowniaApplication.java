package com.sda.javagda14.rowerownia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RowerowniaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RowerowniaApplication.class, args);
    }
}
