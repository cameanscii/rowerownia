package com.sda.javagda14.rowerownia.repository;

import com.sda.javagda14.rowerownia.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {
}
