package com.sda.javagda14.rowerownia.repository;

import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOwner(AppUser appUser);
}
