package com.sda.javagda14.rowerownia.repository;


import com.sda.javagda14.rowerownia.model.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppUserRoleRepository extends JpaRepository<AppUserRole, Long> {
    Optional<AppUserRole> findByName(String name);
}
