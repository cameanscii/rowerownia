package com.sda.javagda14.rowerownia.service;

import com.sda.javagda14.rowerownia.model.Item;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.model.dto.AddItemDto;
import com.sda.javagda14.rowerownia.repository.ItemRepository;
import com.sda.javagda14.rowerownia.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private TaskRepository taskRepository;



    public Optional<Item> add(AddItemDto dto) {
        Optional<Task> taskOptional = taskRepository.findById(dto.getTaskId());
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();

            Item item = new Item();
            item.setName(dto.getName());
            item.setDetails(dto.getDetails());
            item.setCost(dto.getCost());
            item.setTask(task);

            item = itemRepository.save(item);
            task.getItems().add(item);
            Long modifyCost=task.getCost()+dto.getCost();
            task.setCost(modifyCost);

            task = taskRepository.save(task);
            return Optional.of(item);
        }
        return Optional.empty();
    }

    public Optional<Item> getItem(Long itemId) { return itemRepository.findById(itemId);}



    public void delete(Long itemId, Long taskId) {
        Task task=taskRepository.findById(taskId).get();
        Item item=itemRepository.findById(itemId).get();
        task.setCost((task.getCost()-item.getCost()));
        itemRepository.deleteById(itemId);}
}
