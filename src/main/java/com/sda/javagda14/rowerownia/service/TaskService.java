package com.sda.javagda14.rowerownia.service;


import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.Status;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.model.dto.AddTaskToUserDto;
import com.sda.javagda14.rowerownia.model.dto.ModifyTaskDto;
import com.sda.javagda14.rowerownia.repository.AppUserRepository;
import com.sda.javagda14.rowerownia.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private MailingService mailingService;

    public Optional<Task> addTask(AddTaskToUserDto dto) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(dto.getUserId());
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            Task task = new Task();
            task.setDateAdded(LocalDateTime.now());
            task.setDescription(dto.getDescription());
            task.setStatus(Status.PENDING);
            task.setCost(0L);

            task.setOwner(appUser);
            task = taskRepository.save(task);

            appUser.getTaskList().add(task);
            appUser = appUserRepository.save(appUser);

            return Optional.of(task);
        }

        return Optional.empty();
    }


    public List<Task> getAllUserTasks(Long user_id) {
        Optional<AppUser> clientOptional = appUserRepository.findById(user_id);
        if (clientOptional.isPresent()) {
            AppUser appUser = clientOptional.get();
            List<Task> taskList = taskRepository.findAllByOwner(appUser);

            return taskList;
        }
        return new ArrayList<>();
    }

    public List<Task> getAll() {

        return taskRepository.findAll();
    }

    public Optional<Task> getTask(Long taskId) {
        return taskRepository.findById(taskId);
    }

    public Optional<Task> modify(ModifyTaskDto dto) {
        Optional<Task> optionalTask = taskRepository.findById(dto.getIdToModify());
        if (optionalTask.isPresent()) {

            Task task = optionalTask.get();
            task.setStatus(Status.TO_ACCEPT);
            task.setTime(dto.getTime());
            task = taskRepository.save(task);

            mailingService.sendEmail(task.getOwner().getEmail(),"Your task status is currently: TO_ACCEPT");

            return Optional.of(task);
    }
        return Optional.empty();
    }
    public void delete(Long taskId) { taskRepository.deleteById(taskId);}

    public void accept(Long taskId) {
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            task.setStatus(Status.IN_PROGRESS);
            task = taskRepository.save(task);

            mailingService.sendEmail(task.getOwner().getEmail(),"Your task status is currently: IN_PROGRESS");

        }
    }

    public void finish(Long taskId) {
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            task.setStatus(Status.DONE);
            task = taskRepository.save(task);

            mailingService.sendEmail(task.getOwner().getEmail(),"Your task status is currently: DONE");

        }
    }

    public List<Task> getDone(List<Task> tasks) {
        List<Task> tasksDone=new ArrayList<>();

        for (Task task:tasks) {
            if (task.getStatus().name().equals("DONE")){
                tasksDone.add(task);
            }
        }
        return tasksDone;
    }

    public List<Task> getNotDone(List<Task> tasks) {
        List<Task> tasksDone=new ArrayList<>();

        for (Task task:tasks) {
            if (!task.getStatus().name().equals("DONE")){
                tasksDone.add(task);
            }
        }
        return tasksDone;
    }
}
