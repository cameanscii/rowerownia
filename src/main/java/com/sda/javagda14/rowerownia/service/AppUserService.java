package com.sda.javagda14.rowerownia.service;


import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.dto.AddUserDto;
import com.sda.javagda14.rowerownia.repository.AppUserRepository;
import com.sda.javagda14.rowerownia.repository.AppUserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private AppUserRoleRepository appUserRoleRepository;

    @Autowired
    private MailingService mailingService;

    public Optional<AppUser> findByUsername(String email) {
        return appUserRepository.findByEmail(email);
    }

    public List<AppUser> getAll() {
        return appUserRepository.findAll();
    }

    public Optional<AppUser> register(AddUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findByEmail(dto.getEmail());
        if (optionalAppUser.isPresent()) {
            return Optional.empty();
        }

        AppUser newAppUser = new AppUser();
        newAppUser.setName(dto.getName());
        newAppUser.setSurname(dto.getSurname());
        newAppUser.setEmail(dto.getEmail());
        newAppUser.setPassword(encoder.encode(dto.getPassword()));
        newAppUser.setRoles(new HashSet<>(Arrays.asList(appUserRoleRepository.findByName("ROLE_USER").get())));
        newAppUser.setPhone(dto.getPhone());
        newAppUser = appUserRepository.save(newAppUser);

        mailingService.sendEmail(newAppUser.getEmail(),"Welcome in SERWIS ROWEROWY");

        return Optional.of(newAppUser);
    }


    public Optional<AppUser> findById(Long user_id) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(user_id);
        if (optionalAppUser.isPresent()){
            return optionalAppUser;
        }
        return Optional.empty();
    }
}
