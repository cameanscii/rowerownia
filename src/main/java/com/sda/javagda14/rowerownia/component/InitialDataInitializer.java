package com.sda.javagda14.rowerownia.component;

import com.sda.javagda14.rowerownia.model.AppUser;
import com.sda.javagda14.rowerownia.model.AppUserRole;
import com.sda.javagda14.rowerownia.model.Task;
import com.sda.javagda14.rowerownia.repository.AppUserRepository;
import com.sda.javagda14.rowerownia.repository.AppUserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class InitialDataInitializer implements
        ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private AppUserRoleRepository appUserRoleRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        addRole("ROLE_ADMIN");
        addRole("ROLE_USER");

        addAppUser("admin@admin.com", "admin", "ROLE_ADMIN", "ROLE_USER");
        addAppUser("user@user.com", "user", "ROLE_USER");
    }

    @Transactional
    public void addRole(String name) {
        Optional<AppUserRole> appUserRoleOptional = appUserRoleRepository.findByName(name);
        if (!appUserRoleOptional.isPresent()) {
            appUserRoleRepository.save(new AppUserRole(null, name));
        }
    }

    @Transactional
    public void addAppUser(String email, String password, String... roles) {
        Optional<AppUser> appUserOptional = appUserRepository.findByEmail(email);
        if (!appUserOptional.isPresent()) {
            List<AppUserRole> rolesList = new ArrayList<>();
            for (String role : roles) {
                Optional<AppUserRole> employeeRoleOptional = appUserRoleRepository.findByName(role);
                employeeRoleOptional.ifPresent(rolesList::add);
            }

            appUserRepository.save(new AppUser(
                    null, // id
                    null, // name
                    null, // surname
                    passwordEncoder.encode(password), // password
                    email, // email
                    new HashSet<AppUserRole>(rolesList),
                    null,
                    new ArrayList<>()));

        }
    }
}
